/** 启动视频播放器自动播放方法 */
function startVideoComponentAutoPlay() {
	let allVideoComponent = $('.iceTemplateVideo');
	allVideoComponent.each(index => {
		let nowVideo = allVideoComponent[index];
		nowVideo.autoplay = true;
		nowVideo.controls = false;
		nowVideo.loop = true;
		blob_load(nowVideo);
	});
}


const VideoCachesNamespace = 'VideoCachesSpace';
const VideoCachesExpireKey = 'VideoCachesExpireTime';
const VideoCurrentRowId    = 'VideoCachesCurrentRowId';

// 通过blob预加载 src为原视频的视频地址
function blob_load(nowVideo) {
	// 替换真实的播放源地址
	let childrenSource = $(nowVideo.children[0]);
	let tempSrc = childrenSource.attr('tempSrc');
	let videoRowId = tempSrc.substring(tempSrc.lastIndexOf('/') + 1);

	// 检查视频文件是否已经被缓存
	caches.match(tempSrc).then(response => {
		let needToLoadServerData = true;
		// 当前请求地址 客户端存在缓存响应流 则直接获取缓存即可
		if (response) {
            let expireTime = localStorage.getItem(VideoCachesExpireKey);
            // 存在过期时间
            if (expireTime) {

                // 如果当前存储的视频Id 和本次需要加载的Id不同 则直接设置过期时间为0 触发删除事件
                let storeCurrentRowId = localStorage.getItem(VideoCurrentRowId);
                if(storeCurrentRowId !== videoRowId){
                expireTime = 0;
                }

                // 并且缓存时间未到
                if (Date.now() < expireTime) {
                    needToLoadServerData = false;
                    console.log('存在缓存,并且没有过期-->', tempSrc , '||||||||||||||||过期时间-->' , new Date(expireTime));
                    needToLoadServerData = false;
                    response.blob().then(blob => {
                        // 渲染页面Video
                        let blobSrc = URL.createObjectURL(blob);
                        $(childrenSource).attr('src', blobSrc);
                        nowVideo.load();
                    });
                }
                // 缓存已过期 , 则清除原来的caches
                else {
                    console.log('清除缓存信息-->', tempSrc);
                    caches.open(VideoCachesNamespace).then(cache => {
                        cache.delete(tempSrc);
                    });
                }
			}
		}

		// 需要加载服务器数据 可能原因  1. 不存在缓存 2. 存在缓存但是过期了
		if (needToLoadServerData) {
			console.log('需要加载服务器数据-->', tempSrc);
			// 发起请求获取视频数据
			fetch(tempSrc)
				.then(response => {
					if (!response.ok) {
						throw new Error('网络响应不是 ok');
					}
					return response.blob(); // 将响应转换为 Blob 对象
				})
				.then(blob => {
					// 将视频数据存储到浏览器的本地存储中
					caches.open(VideoCachesNamespace).then(cache => {
						const response = new Response(blob, {
							headers: {
								'Content-Type': 'video/mp4'
							}
						});
						// 当前请求的响应添加至缓存
						cache.put(tempSrc, response);

						// 设置localstore过期时间 , 默认为 7天
						let expireTime = Date.now() + 5 * 1000;
						localStorage.setItem(VideoCachesExpireKey, expireTime);

            // 设置localstore当前视频Id
            localStorage.setItem(VideoCurrentRowId,videoRowId);

						// 渲染页面Video
						let blobSrc = URL.createObjectURL(blob);
						$(childrenSource).attr('src', blobSrc);
						nowVideo.load();
					});
				})
				.catch(error => {
					console.error('获取视频数据时发生错误：', error);
				});
		}
	});
}
